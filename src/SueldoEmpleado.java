
public class SueldoEmpleado {
    private double horasTrabajadas;
    private double horasExtras;
    private double sueldoBruto;
    private double descuentoSalud;
    private double descuentoAFP;

    private int valorHora = 5000;
    private int valorHoraExtra = 7000;
    private double SueldoBruto;

    public SueldoEmpleado(){

    }

    public SueldoEmpleado(double ht, double he, double ds, double da){
        this.horasTrabajadas = ht;
        this.horasExtras = he;
        this.descuentoAFP = da;
        this.descuentoSalud = ds;
        this.sueldoBruto = (this.horasTrabajadas * this.valorHora)+ (this.horasExtras*this.valorHoraExtra);
    }
    //setters

    public void setHorasTrabajadas(double ht){
        this.horasTrabajadas = ht;
    }

    public void setHorasExtras(double he){
        this.horasExtras = he;
//quepasaloco
    }

    public void setDescuentoAFP(double da) {
        this.descuentoAFP = da;
    }

    public void setDescuentoSalud(double ds){
        this.descuentoSalud = ds;
    }

    //Getters
    public double gethorasTrabajadas(){
        return this.horasTrabajadas;
    }

    public double gethorasExtras (){
        return this.horasExtras;
    }

    public double getDescuentoSalud(){
        return this.descuentoSalud;
    }

    public double getDescuentoAFP (){
        return this.descuentoAFP;
    }

    //Metodos

    public double calcularSueldoBruto(){
        this.sueldoBruto = (this.horasTrabajadas * this.valorHora)+ (this.horasExtras*this.valorHoraExtra);
        return this.sueldoBruto;
    }

    public double totaldescuentoAFP(){
        return this.descuentoAFP*this.sueldoBruto;
    }
    public double totalDescuentoSalud(){
        return this.descuentoSalud*this.sueldoBruto;
    }
    public double totalBonoMarzo(){
        if(this.sueldoBruto>200000){
            return this.sueldoBruto*0.2;
        }else {
            return this.sueldoBruto*0.15;
        }
    }
}